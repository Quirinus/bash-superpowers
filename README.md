# Bash superpowers

Assortment of various aliases, functions, .bashrc/.profile settings, etc. that I find convenient.

### Before using
- make sure to change the timezone in the `now` alias, and the location of the error log file in the `error` alias
- add your git name and email to `.profile`
- any aliases/functions related to the server go to `.bash_aliases_server_specific`, `.bash_aliases` holds just general aliases/functions
- all remotes are "origin", and all master branches are "main", you can change them in the files, if needed

### General features
- classic features like history, autocompletion, clearing console on logout
- sets your git username and email on login, and commit signing if you have a public gitlab key
- remembers your ssh password for the session, after you enter it once on login, so you don't have to type it like a monkey every time you do git commands
- clears screen on login (you can see everything after login, but before entering the ssh password), and shows current directory (home) path and content
- shows a pimped up minimalistic prompt, only a red `>> `, unless you're in a git repository - then it shows the branch name and if you're behind `<`, ahead `>`, or up-to-date `=`, and more

### Aliases and functions
Classic:
- `l` - nice `ls`, also shows current directory
- `ld` - like `ls`, but shows a detailed list (owner, group, permissions, size, timestamps)
- `c` - for `clear`, also shows current directory and directory content
- `mk` - `mkdir` then enters the created directory (creates parent directories if needed)
- `r` - short for the lovely `rm -rf`, happy deleting!
- `per` - shows human readable permissions separated by "|", plus the permission number
- `owner` - shows the owners of a file
- `own` or `own \path\to\file\or\directory` - change ownership (recursively) of current directory or a file/directory in a path to your user
- `www` or `www \path\to\file\or\directory` - change ownership (recursively) of current directory or a file/directory in a path to `www-data:www-data`
- `e` - extracts a bunch of different archive formats
- `s` - gets into the superuser in the current directory, keeping the user settings (e.g. prompt style and aliases)
- `ssudo` - super `sudo` - run aliases, functions, etc. of the current user, but as sudo
- `utc`/`now`/`servertime`/`timestamp` - current utc time, current local time (update your timezone in the alias), server time, unix timestamp
- `t` or `t <session number>` - list tmux sessions or attach a session by its number, if argument specified

Searching/counting/:
- `gall`/`fall`/`gfall` - grep file contents, find file/directory by name, or both (all recursively, and nicely formatted)
- `countf`/`countd` - counts the number of files/directories in current directory and children
- `sizef`/`sized`/`sizeall` - file/directory/both sizes in current directory, and the total, sorted descending
- `timef`/`timed` - file/directory sizes in current directory, sorted descending

Movement:
- `cd` - shows current directory and directory contents automatically on entering
    - `cd` - accepts partial matches
- `..` - go up one directory
- `,,` - go to the previous directory (calling it again returns to the previous path)
- `...` - go to root directory
- `home`, `h` - go to home directory

Hashing:
- `pass <length>` - generate random password of specific length
- `md5`/`sha1`/`sha256` - get file hashes
- `integrity` - integrity hash for js/css files

Certificate commands:
- `cert`, `certshort` - show all or only the most important certificate information
- `chainkey`, `certkey`, `chain`, others - check if certificate chain, certificate, and key match
- `csrkeygencn`, `csrkey` - generate/check a CSR file and a private key (this only adds CN)
- `certsitedate` - check the before and after dates (creation and expiration) dates of a remote site

Git commands:
- `top` - goes to the top directory of the git repository
- `reset` - pick files from git status to reset to their last commit
- `edit` - pick files to edit from git diff list - edits multiple files (close one to edit the next), and goes to the line of first change in each file
- `push`/`pull`/`commit`/`fetch` - classic `git` commands, without the `git` prefix
- `add`/`addall` - adds only already included files, or all files (of course, you can still use the regular `git add <path>`)
- `go <message>` - does `pull`, `add`, `commit` (with `<message>`), and `push`
- `status` - `fetch` followed by a short, but detailed status
- `log`/`logr` - `fetch` followed by a nicely formatted `git log` for local/remote repository
- `diff`/`diffs`/`diffr`/`diffstash` - diff of latest local commit, staged files, remote (after doing fetch), or stash
- `back`/`main` - goes back one commit (can be used multiple times), or to the latest commit on main branch
- `amend`/`amendm`/`amendf`/`amenda` - normal amend, amend message only, amend files only, amend author
- `tag <name>` - does `fetch`, creates a lightweight tag with `<name>`, and then pushes it to remote
- `ugit`, `gitconf`, `gitlag_ssh` - create git user, configure it, and create gitlab keys + commit signing

Server commands:
- creating/removing groups and users from them
- `ussh` - adding ssh access for a user (creates private and public keys, adds to authorized_keys)
- `disable_password`, `disable_root_login` - disabling user/pass and root user logins

PHP:
- `error` - gets last 20 lines of the php error log, formatted for easier reading (you have to update the error log location in the alias)

There's also some other aliases/functions that are in the files, but are not listed here. To run commands that have been replaced by them, you can use `command <name> <args>`.
