# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

# Load aliases
if [ -f ~/.aliases/.bash_aliases ]; then
    source ~/.aliases/.bash_aliases
fi

for file in ~/.aliases/.bash_aliases_*; do
    source "$file"
done

# Definition for input handling.
# E.g. you can use it to define Ctrl+Left Arrow and Ctrl+Right Arrow
# to skip words.

if [ -f ~/.inputrc ]; then
    bind -f  ~/.inputrc
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# INSTEAD OF THIS, USE AGENT FORWARDING! E.G. PAEGANT
# # ask for ssh password only once per session:
# killall "ssh-agent -s"
# eval `ssh-agent -s` 1>/dev/null;
# ssh-add -t 8h ~/.ssh/id_rsa;
# trap "ssh-agent -k" exit;
# c;

# ask for ssh password only once per session,
# and don't start more agents if one is already running:
#if [ $(ps ax -U $USER | grep [s]sh-agent | wc -l) -gt 0 ] ; then
#    echo "ssh-agent is already running"
#else
#    eval $(ssh-agent -s) 1>/dev/null;
#    if [ "$(ssh-add -l)" == "The agent has no identities." ] ; then
#        ssh-add -t 2h ~/.ssh/id_rsa
#    fi
#
#    # Don't leave extra agents around: kill it on exit. You may not want this part.
#    # trap "ssh-agent -k" exit
#fi
l;
