# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

NAME="Ivan Jelenić"
EMAIL="ivan.jelenic42@gmail.com"

git config --global user.name "$NAME"
git config --global user.email "$EMAIL"

GITLAB_RSA="$HOME/.ssh/id_rsa_gitlab"
GIT_CONF_DIR="$HOME/.config/git"
ALLOWED_SIGNERS="$GIT_CONF_DIR/allowed_signers"
if [ -f "$GITLAB_RSA.pub" ]; then
    git config --global user.signingkey "$GITLAB_RSA.pub"
    git config --global gpg.format ssh
    git config --global commit.gpgSign true

    # Ensure the allowed signers file exists and contains the SSH public key
    mkdir -p "$GIT_CONF_DIR"
    touch "$ALLOWED_SIGNERS"
    if ! grep -q -F "$(cat "$GITLAB_RSA.pub")" "$ALLOWED_SIGNERS"; then
        echo "$EMAIL $(cat $GITLAB_RSA.pub)" >> "$ALLOWED_SIGNERS"
    fi
    git config --global gpg.ssh.allowedSignersFile "$ALLOWED_SIGNERS"
else
    git config --global --unset user.signingkey
    git config --global --unset gpg.format
    git config --global commit.gpgSign false
    git config --global --unset gpg.ssh.allowedSignersFile
    rm -f "$ALLOWED_SIGNERS"
    echo "If you would like for your git commits to be signed,"
    echo "add your public gitlab key to ~/.ssh/id_rsa_gitlab.pub"
    echo "You can find the public key in:"
    echo "GitLab -> Profile Settings -> SSH Keys -> open a key -> copy SSH Key."
    echo "It has to have Authentication and Signing as it's Usage type."
    echo "If you don't have one, you can run gitlab_ssh to create one."
fi
git update-index --assume-unchanged .aliases/.bash_aliases_custom
gitconf
# git config pull.ff only

# https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh
#source ~/.git-prompt.sh

GIT_PS1_SHOWDIRTYSTATE=1
#GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWUNTRACKEDFILES=1
GIT_PS1_SHOWUPSTREAM="auto"
#GIT_PS1_STATESEPARATOR=" "
#GIT_PS1_COMPRESSSPARSESTATE=1
#GIT_PS1_OMITSPARSESTATE=1
#GIT_PS1_DESCRIBE_STYLE="default"
GIT_PS1_SHOWCOLORHINTS=1
#GIT_PS1_HIDE_IF_PWD_IGNORED=1

pull
aliasreload

# List screen sessions if there are any
if screen -ls | grep -q "\.pts"; then
    screen -ls
fi

# List tmux sessions if there are any.
if tmux has-session 2>/dev/null; then
  tmux ls
fi


#export PS1='\e[32m[\h]\e[33m$(__git_ps1 " (%s)")\e[0m >> '
#export PS1='\[\e[32m\]$(__git_ps1 "[%s] ")\[\033[0;31m\]>>\[\e[0m\] '
export PS1='\[\e[32m\]>>\[\e[0m\] '