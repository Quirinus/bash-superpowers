#!/bin/bash

# **** Git related aliases **** # use this instead? https://kevcui.github.io/2020/07/04/git-alias-did-you-git-it/
alias top='cd `git rev-parse --show-toplevel`' # go to top folder of Git repository
alias push='git push'
alias pushup='git push -u' # specify the upstream branch, if it doesn't exist upstream, it creates it. use this when you create a local branch and want to create/push it to upstream.
alias pull='git pull'
alias status='git fetch && git status --short --branch --show-stash'
add() {
    if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
        echo "Usage: add [FILE]"
        echo
        echo "Without arguments: Stages only modified and deleted files."
        echo "With a file path:  Stages the specified file."
        return 0
    fi

    if [ "$#" -eq 0 ]; then
        # Add all modified and deleted files, but not new files
        git add --update
        echo "Modified and deleted files staged successfully."
    else
        # Add the specified file
        git add "$1"
        echo "File '$1' staged successfully."
    fi

    status
}
alias patch='git add --patch' # choose which parts/files to add one-by-one
alias add.='git add .' # stages modified and new files - not deleted files
alias addall='git add -A && status' # stages everything - new, modified and deleted files
alias addnew='git ls-files --others --exclude-standard | xargs git add && status'
alias addmodified='git diff --name-only --diff-filter=M | xargs git add && status'
alias adddeleted='git diff --name-only --diff-filter=D | xargs git add && status'
alias untracked='git ls-files . --exclude-standard --others --directory -t' # shows untracked/new files
alias ignored='git ls-files . --ignored --exclude-standard --others --directory -t' # shows ignored files
alias commit='git commit -m'
#commit() { # commit without having to doublequote # doesn't work, have to figure out how to escape variables
#    builtin printf '%q' "$*" 1>"$message";
#    git commit -m "$message";
#}
go() { # git adds, commits and pushes
    pull && add && commit "$1" && push;
    status
}
alias fetch='git fetch'
alias log='git fetch && echo "Last 10 commits:" && git log --pretty=format:"%C(Yellow)%h%Creset | %><(14)%C(dim)%ar%Creset | %an%Creset <%C(magenta)%ae%Creset> | %C(cyan)%s%Creset %C(Green)%d" -n 10' # https://stackoverflow.com/a/36676939
alias logr='git fetch && echo "Last 10 commits:" && git log origin/main --pretty=format:"%C(Yellow)%h%Creset | %><(14)%C(dim)%ar%Creset | %an%Creset <%C(magenta)%ae%Creset> | %C(cyan)%s%Creset %C(Green)%d" -n 10 || echo "Using origin/master instead." && git log origin/master --pretty=format:"%C(Yellow)%h%Creset | %>'
reset() {
    # Display the help/usage information
    if [[ "$1" == "--help" || "$1" == "-h" ]]; then
        echo "Usage: reset [OPTION/FILE]"
        echo "Custom git function to simplify common tasks."
        echo
        echo "Without arguments:        Reset the current branch to the latest commit (git reset --hard) or pick a specific file to restore from the list (git checkout HEAD -- \"filename\")."
        echo "With a filename argument: Restore the file to its state in the latest commit (git checkout HEAD -- \"filename\")."
        echo
        echo "Untracked files can be picked to be deleted (after confirmation)."
        return 0
    fi

    # If no arguments are provided
    if [ "$#" -eq 0 ]; then
        # Get the status of the repository including the branch and stash with color
        git_status=$(git -c color.status=always status --short --branch --show-stash)
        branch_info=$(echo "$git_status" | grep '^#')

        # Get the list of changed files
        changed_files=$(git status --porcelain | awk '{print $2}')

        if [ -z "$changed_files" ]; then
            echo "$git_status"
            echo "No changed files found."
            return
        fi

        echo -e "$branch_info"
        echo "Changed files:"
        echo "[0]/[Enter] Reset the entire branch to the latest commit (git reset --hard)"
        index=1
        while IFS= read -r line; do
            # Skip branch information lines
            if [[ $line == \#* ]]; then
                continue
            fi
            echo -e "[$index] $line"
            index=$((index + 1))
        done <<< "$git_status"

        # Prompt the user to choose files to reset
        echo "Enter the numbers of the files you want to reset (git checkout HEAD -- \"filename\"), separated by spaces"
        read -p "or [0]/[Enter] for hard reset (git reset --hard): " file_numbers

        # Check for empty input (Enter key)
        if [ -z "$file_numbers" ]; then
            file_numbers=0
        fi

        # Validate the input
        for number in $file_numbers; do
            if ! echo "$number" | grep -qE '^[0-9]+$'; then
                echo "Invalid input: $number. Please enter numbers separated by spaces."
                return
            fi
        done

        # Check if 0 is in the list of numbers for hard reset
        if [[ " $file_numbers " == *" 0 "* ]]; then
            if [[ "$file_numbers" == "0" ]]; then
                git reset --hard
                echo "Branch has been reset to the latest commit."
            else
                echo "You cannot select 0 (hard reset) along with other file numbers. Please choose one or the other."
            fi
            return
        fi

        # Reset the chosen files
        for file_number in $file_numbers; do
            chosen_file=$(echo "$changed_files" | sed -n "${file_number}p")
            
            if [ -z "$chosen_file" ]; then
                echo "Invalid choice. No file corresponds to the entered number: $file_number"
                continue
            fi

            # Check if the file is tracked by Git
            if ! git ls-files --error-unmatch "$chosen_file" > /dev/null 2>&1; then
                read -p "File '$chosen_file' is not tracked by Git and will be deleted. Are you sure? [y/N]: " confirm
                if [[ "$confirm" =~ ^[Yy]$ ]]; then
                    rm "$chosen_file"
                    echo "File '$chosen_file' has been deleted."
                else
                    echo "File '$chosen_file' has not been deleted."
                fi
                continue
            fi

            git checkout HEAD -- "$chosen_file"
            echo "File '$chosen_file' has been reset."
        done

    # If one argument is provided
    elif [ "$#" -eq 1 ]; then
        # Check if the file is tracked by Git
        if ! git ls-files --error-unmatch "$1" > /dev/null 2>&1; then
            read -p "File '$1' is not tracked by Git and will be deleted. Are you sure? [y/N]: " confirm
            if [[ "$confirm" =~ ^[Yy]$ ]]; then
                rm "$1"
                echo "File '$1' has been deleted."
            else
                echo "File '$1' has not been deleted."
            fi
            return
        fi

        git checkout HEAD -- "$1"
        echo "File '$1' has been reset."
    else
        echo "Invalid number of arguments."
        echo "Run 'reset --help' for usage."
    fi
}

edit() {
    # Display the help/usage information
    if [[ "$1" == "--help" || "$1" == "-h" ]]; then
        echo "Usage: edit_files"
        echo "Custom git function to list changed files and edit selected ones."
        echo "It will jump to the first changed line."
        echo
        echo "Without arguments: List changed files and open selected ones in nano for editing."
        return 0
    fi

    # If no arguments are provided
    if [ "$#" -eq 0 ]; then
        # Get the status of the repository including the branch and stash with color
        git_status=$(git -c color.status=always status --short --branch --show-stash)
        branch_info=$(echo "$git_status" | grep '^#')

        # Get the list of changed files
        changed_files=$(git diff --name-status -D --minimal --abbrev HEAD)

        if [ -z "$changed_files" ]; then
            echo "$git_status"
            echo "No changed files found."
            return
        fi

        echo -e "$branch_info"
        echo "Changed files:"
        index=1
        declare -A file_map
        while IFS= read -r line; do
            # Skip branch information lines
            if [[ $line == \#* ]]; then
                continue
            fi
            echo -e "[$index] $line"
            file_map[$index]=$(echo "$line" | awk '{print $2}')
            index=$((index + 1))
        done <<< "$changed_files"

        # Prompt the user to choose files to edit
        echo "Enter the numbers of the files you want to edit, separated by spaces:"
        read -p "" file_numbers

        # Validate the input
        for number in $file_numbers; do
            if ! echo "$number" | grep -qE '^[0-9]+$'; then
                echo "Invalid input: $number. Please enter numbers separated by spaces."
                return
            fi
        done

        # Prepare the command to open the selected files in nano
        nano_command="nano"
        for file_number in $file_numbers; do
            chosen_file=${file_map[$file_number]}
            if [ -z "$chosen_file" ]; then
                echo "Invalid choice. No file corresponds to the entered number: $file_number"
                continue
            fi
            first_changed_line=$(git diff -U0 "$chosen_file" | grep -Po '^@@ .+ \+\K[0-9]+' | head -1)
            nano_command+=" +${first_changed_line} ${chosen_file}"
        done

        # Execute the nano command
        if [ "$nano_command" != "nano" ]; then
            eval "$nano_command"
        else
            echo "No valid files selected."
        fi

    # If arguments are provided (although not used in this function)
    else
        echo "Invalid number of arguments."
        echo "Run 'edit_files --help' for usage."
    fi
}

alias clean="git clean -f"
alias unstage="git restore --staged ."
cache() {
    git rm -r --cached .
    git add .
    status
}
alias bisect='git bisect start' # bisect commits until you find the last working (or not working) commit
alias diff='git diff -D --minimal --abbrev'
alias difff='git diff -D --minimal --abbrev HEAD' # --name-only # full diff
alias diffs='git diff -D --minimal --abbrev --staged' # --name-only
alias diffr='git diff -D master origin/master || git diff -D main origin/main' # get diff to remote
alias diffstash='git stash show -p' # diff to stash
compare() { # can compare any two commits/branches, local or remote, ...
    git diff $1..$2;
}
alias diffc='compare'
alias back='git checkout HEAD~1' # move back one commit, to get back use main
alias main='git checkout main' # returns to the latest commit, if the master branch is called main
alias master='git checkout master' # returns to the latest commit, if the master branch is called master
alias amendall='git commit --amend' # amend the files and message, like a new commit replacing the previous one
alias amend='git commit --amend -m' # just change the commit message, not files
alias amendfiles='git commit --amend --no-edit' # amend files, but dont change the commit message
amendauthor() {
    git commit --amend --no-edit --author="$1 <$2>";
    # git push -f origin main # if the commit was pushed
}
alias stash='git stash'
alias stashp='git stash pop'
alias stashpop='stashp'
alias checkout='git checkout'
alias check='git checkout'
alias clone='git clone'
clonein() {
    git init
    git remote add origin "$1"
    git pull
    git checkout main || git checkout master
    status
}
alias clone.='clonein'
cloneninf() {
    git init
    git remote add origin "$1"
    git pull
    git checkout main -f || git checkout master -f
    status
}
alias clonef.='cloneinf'
alias remote='git remote -v'
githelp() {
    git $1 -h;
}
alias rebase='git rebase @{u}' # rebase in case of commits done before pulling, to avoid useless merging
alias cancel='git reset --soft HEAD~1' # undo last commit, but leave changes. use --hard to remove changes

ugit() {
    if [ "$#" -eq 2 ]; then
        # If two arguments are passed, set Git config for the current user
        local git_username="$1"
        local git_email="$2"

        git config --global user.name "$git_username"
        git config --global user.email "$git_email"
        echo "Git username and email set for current user."

    elif [ "$#" -eq 3 ]; then
        # If three arguments are passed, set Git config for the specified user
        local target_user="$1"
        local git_username="$2"
        local git_email="$3"

        # Check if the user exists
        if id "$target_user" &>/dev/null; then
            sudo -u "$target_user" git config --global user.name "$git_username"
            sudo -u "$target_user" git config --global user.email "$git_email"
            echo "Git username and email set for $target_user"
        else
            echo "User $target_user does not exist."
            return 1
        fi

    else
        echo "Usage:"
        echo "- For current user: ugit <git_username> <email>"
        echo "- For other user: ugit <target_username> <git_username> <email>"
        return 1
    fi
}
gitconf() {
    # If no arguments provided, try to fetch existing git username and email
    if [ "$#" -eq 0 ]; then
        local username=$(git config --global user.name)
        local email=$(git config --global user.email)

        if [ -z "$username" ] || [ -z "$email" ]; then
            echo "Git username or email is not set. Please provide them as arguments."
            return 1
        fi
    elif [ "$#" -ne 2 ]; then
        echo "Usage: gitconf [<username> <email>]"
        return 1
    else
        local username="$1"
        local email="$2"

        # Set username and email
        git config --global user.name "$username"
        git config --global user.email "$email"
    fi

    # Other configurations
    git config --global core.editor "nano"
    git config --global color.ui true
    git config --global init.defaultBranch main
    git config --global delta.line-numbers true
    # git config --global push.default simple
    # git config --global pull.rebase true # rebase or merge on pull
    git config --global credential.helper 'cache --timeout=3600' # Cache the password for that many seconds
    # git config --global merge.tool meld
    # git config --global help.autocorrect 10
    git config --global core.fileMode false # Ignore file permission changes

    echo "Git configurations have been set up!"
}
alias gitconfig='gitconf'
alias git_config='gitconf'

gitlab_ssh() {
  # Define paths for the new key
  GITLAB_RSA="$HOME/.ssh/id_rsa_gitlab"

  # Check if the key already exists
  if [[ -f $GITLAB_RSA ]]; then
    echo "The GitLab-specific SSH key already exists!"
    return 1
  fi

  # Create the .ssh directory if it doesn't exist and set temporary permissions to 700
  mkdir -p "$HOME/.ssh"
  chmod 700 "$HOME/.ssh"

  # Generate the new SSH key for GitLab
  ssh-keygen -t rsa -b 4096 -f $GITLAB_RSA -C "gitlab-$(whoami)@$(hostname)"

  # Set permissions to the generated keys
  chmod 400 "$GITLAB_RSA"
  chmod 400 "${GITLAB_RSA}.pub"

  # Check if ${HOME}/.ssh/config exists and if so, set permissions and then backup
  if [[ -f "$HOME/.ssh/config" ]]; then
    # Set permissions to allow reading and writing
    chmod 600 "$HOME/.ssh/config"

    # Backup existing config
    cp "$HOME/.ssh/config" "$HOME/.ssh/config.bak"
  fi

  # Prepend the GitLab configuration to a temporary config file (because order matters)
  echo "# GitLab Specific Identity" > "$HOME/.ssh/config_temp"
  echo "Host gitlab.com" >> "$HOME/.ssh/config_temp"
  echo "  IdentityFile $GITLAB_RSA" >> "$HOME/.ssh/config_temp"
  echo "  IdentitiesOnly yes" >> "$HOME/.ssh/config_temp"

  # If ${HOME}/.ssh/config exists, append its contents to the temporary config file
  if [[ -f "$HOME/.ssh/config" ]]; then
    cat "$HOME/.ssh/config" >> "$HOME/.ssh/config_temp"
  fi

  # Replace ~/.ssh/config with the temporary config file
  mv "$HOME/.ssh/config_temp" "$HOME/.ssh/config"

  # Set permissions to read-only for ~/.ssh/config and set ~/.ssh back to 500
  chmod 400 "$HOME/.ssh/config"
  chmod 500 "$HOME/.ssh"

  echo "Configuration added to ~/.ssh/config for GitLab with the new SSH key."

  # Enable commit signing.
  git config --global user.signingkey "$GITLAB_RSA.pub"
  git config --global gpg.format ssh
  git config --global commit.gpgSign true

  # Ensure the allowed signers file exists and contains the SSH public key
  GIT_CONF_DIR="$HOME/.config/git"
  mkdir -p "$GIT_CONF_DIR"
  ALLOWED_SIGNERS="$GIT_CONF_DIR/allowed_signers"
  touch "$ALLOWED_SIGNERS"

if [ -z "$EMAIL" ]; then
  GIT_EMAIL=$(git config --get user.email)
  if [ -z "$GIT_EMAIL" ]; then
    EMAIL="*"
    echo "Please set your email. Using wildcard instead of email for allowing commit signers. Then replace the * in $ALLOWED_SIGNERS."
  else
    EMAIL="$GIT_EMAIL"
  fi
fi

  if ! grep -q -F "$(cat "$GITLAB_RSA.pub")" "$ALLOWED_SIGNERS"; then
    echo "$EMAIL $(cat $GITLAB_RSA.pub)" >> "$ALLOWED_SIGNERS"
  fi
  git config --global gpg.ssh.allowedSignersFile "$ALLOWED_SIGNERS"

  # Output the public key and provide instruction to user
  echo "----------------------------------------------------"
  echo "Please add the following public key to your GitLab account:"
  echo "----------------------------------------------------"
  cat "${GITLAB_RSA}.pub"
  echo "----------------------------------------------------"
  echo "Go to GitLab -> Profile Settings -> SSH Keys and add the above key. Pick Authentication and Signing."
}
