#!/bin/bash

# alias users='cat /etc/passwd'
alias users='getent passwd'
alias usersnormal='getent passwd {1000..60000}'
alias groups='getent group'
unew() { # creates a new user with a password
    if [ "$#" -ne 2 ]; then
        echo "Usage: unew <username> <password>"
        return 1
    fi

    local username="$1"
    local password="$2"

    # Create a new group with the same name as the username
    sudo groupadd "$username"

    # Add the user
    sudo useradd -g "$username" -G sudo,docker -s /bin/bash -m "$username"

    # Check if user creation was successful
    if [ $? -eq 0 ]; then
        echo "$username:$password" | sudo chpasswd
        if [ $? -ne 0 ]; then
            echo "Failed to set password for $username"
            return 1
        fi
        echo "User $username created and password set successfully!"
    else
        echo "Failed to create user $username"
        return 1
    fi
}
alias ug='usermod -a -G' # usage: ug group1,group2 username
alias ugrem='sudo gpasswd -d' # usage: ugrem username groupname'
ussh() { # add ssh access for current user if no arguments, or specify the target user
    local username="$1"
    local cmd_prefix=""

    # If no arguments are supplied, set the username to the current user
    if [ "$#" -eq 0 ]; then
        username="$USER"
    elif [ "$#" -ne 1 ]; then
        echo "Usage: ussh [<username>]"
        return 1
    fi

    # Use sudo only if target username is different from the current user
    if [ "$username" != "$USER" ]; then
        cmd_prefix="sudo -u $username"
    fi

    # Ensure the .ssh directory exists
    $cmd_prefix mkdir -p /home/"$username"/.ssh

    # Set temporary permissions and ownership for the .ssh directory
    $cmd_prefix chown -R "$username":"$username" /home/"$username"/.ssh
    $cmd_prefix chmod 700 /home/"$username"/.ssh

    # Generate the SSH key pair for the user
    $cmd_prefix ssh-keygen -t rsa -b 4096 -f /home/"$username"/.ssh/id_rsa

    # Create the authorized_keys file
    $cmd_prefix touch /home/"$username"/.ssh/authorized_keys

    # Temporarily adjust permissions to allow appending to authorized_keys
    $cmd_prefix chmod 600 /home/"$username"/.ssh/authorized_keys

    # Add the public key to authorized_keys for the user
    $cmd_prefix cat /home/"$username"/.ssh/id_rsa.pub >> /home/"$username"/.ssh/authorized_keys

    # Adjust permissions and ownership for the .ssh directory and its contents
    $cmd_prefix chmod 500 /home/"$username"/.ssh
    $cmd_prefix chmod 400 /home/"$username"/.ssh/{authorized_keys,id_rsa,id_rsa.pub}

    echo "SSH key pair generated and set up for $username!"
    echo "Private key location: /home/$username/.ssh/id_rsa"
}

disable_password() { # disables normal login using account password
    # Check if the configuration file has the PasswordAuthentication directive
    if grep -q "^PasswordAuthentication" /etc/ssh/sshd_config; then
        # If it exists, set it to 'no'
        sudo sed -i 's/^PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
    else
        # If it doesn't exist, append it to the file
        echo "PasswordAuthentication no" | sudo tee -a /etc/ssh/sshd_config > /dev/null
    fi

    # Restart the SSH service to apply changes
    sudo systemctl restart sshd

    echo "SSH password authentication has been disabled."
}

disable_root_login() {
    # Check if the configuration file has the PermitRootLogin directive
    if grep -q "^PermitRootLogin" /etc/ssh/sshd_config; then
        # If it exists, set it to 'no'
        sudo sed -i 's/^PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
    else
        # If it doesn't exist, append it to the file
        echo "PermitRootLogin no" | sudo tee -a /etc/ssh/sshd_config > /dev/null
    fi

    # Restart the SSH service to apply changes
    sudo systemctl restart sshd

    echo "Logging in as the root user has been disabled."
}

# **** SSL certificates ***
certmd5() { openssl x509 -modulus -noout -in "$1" | openssl md5; }
keymd5() { openssl rsa -modulus -noout -in "$1" | openssl md5; }
csrmd5() { openssl req -modulus -noout -in "$1" | openssl md5; }
certkey() {
    certmd5=$(certmd5 $1)
    keymd5=$(keymd5 $2)
    if [ "$certmd5" = "$keymd5" ]; then
        echo "Certificate and key md5 hashes match: $certmd5"
    else
       echo "Certificate and key md5 hashes don't match."
       echo "Certificate md5 hash: $certmd5"
       echo "Key md5 hash:         $keymd5"
    fi
}
csrkey() {
    csrmd5=$(csrmd5 $1)
    keymd5=$(keymd5 $2)
    if [ "$csrmd5" = "$keymd5" ]; then
        echo "CSR and key md5 hashes match: $csrmd5"
    else
       echo "CSR and key md5 hashes don't match."
       echo "CSR md5 hash: $csrmd5"
       echo "Key md5 hash: $keymd5"
    fi

}
chain() { openssl verify -CAfile "$1" "$2"; }
chainkey() { # usage: chain CA_interim_cert.cer cert.cer private.key
    chain=$(chain $1 $2)
    certmd5=$(certmd5 $2)
    keymd5=$(keymd5 $3)
    if [ "$certmd5" = "$keymd5" ] && [ "$chain" = "$2: OK" ]; then
        echo -e "OK\n"

        cacertdate=$(certdate $1)
        echo -e "CA interim certificate expiration date: $cacertdate\n"

        certdate=$(certdate $2)
        echo "Certificate expiration date:            $certdate"
        openssl x509 -noout -subject -in $2
        openssl x509 -noout -ext subjectAltName -in $2
    elif [ "$certmd5" != "$keymd5" ] && [ "$chain" != "$2: OK" ]; then
        echo "Nothing matches.."
    elif [ "$certmd5" != "$keymd5" ]; then
        echo "Certificate and private key don't match."
    elif [ "$chain" != "$2: OK" ]; then
        echo "CA certificate doesn't match the certificate."
    fi
}

pkcs7() { openssl pkcs7 -in "$1" -print_certs -out "$2"; } # extract certificates from a PKCS7 file
alias cert='openssl storeutl -noout -text -certs'
certshort() {
  if [ -z "$1" ]; then
    echo "Usage: display_certificates_info <path_to_cert_file>"
    return 1
  fi

  trim() {
    local var="$1"
    var="${var#"${var%%[![:space:]]*}"}"
    var="${var%"${var##*[![:space:]]}"}"
    echo -n "$var"
  }

  process_cert() {
    local cert="$1"
    local cert_count="$2"

    issuer=$(echo "$cert" | openssl x509 -noout -issuer 2>/dev/null | sed 's/^[^=]*=//')
    subject=$(echo "$cert" | openssl x509 -noout -subject 2>/dev/null | sed 's/^[^=]*=//')
    san=$(echo "$cert" | openssl x509 -noout -ext subjectAltName 2>/dev/null | sed 's/^X509v3 Subject Alternative Name://')
    not_before=$(echo "$cert" | openssl x509 -noout -startdate 2>/dev/null | sed 's/^[^=]*=//')
    not_after=$(echo "$cert" | openssl x509 -noout -enddate 2>/dev/null | sed 's/^[^=]*=//')
    md5_modulus=$(echo "$cert" | openssl x509 -noout -modulus 2>/dev/null | openssl md5 | sed 's/^[^=]*= //')

    echo "----- Certificate $cert_count -----"
    echo "Issuer:       $issuer"
    echo "Subject:      $subject"
    if [[ -n "$(trim "$san")" ]]; then
      echo "Subject Alternative Name: $san"
      echo
    fi

    echo
    echo "Not Before:   $not_before"
    echo "Not After:    $not_after"
    echo
    echo "Modulus MD5:  $md5_modulus"
    echo
  }

  cert_file="$1"
  cert_count=0
  cert=""

  echo

  while IFS= read -r line || [ -n "$line" ]; do
    cert+="$line"$'\n'
    if [[ $(trim "$line") == "-----END CERTIFICATE-----" ]]; then
      cert_count=$((cert_count + 1))
      process_cert "$cert" "$cert_count"
      cert=""
    fi
  done < "$cert_file"

  # Check if there is any remaining certificate data after the loop
  if [[ -n "$(trim "$cert")" ]]; then
    cert_count=$((cert_count + 1))
    process_cert "$cert" "$cert_count"
  fi

  echo "___________________________________"
  echo "Total number of certificates: $cert_count"
  echo
}

certdate() { cat "$1" | openssl x509 -noout -enddate; }
certsitedate() { echo | openssl s_client -servername $1 -connect $1:443 | openssl x509 -noout -enddate; } # -dates
alias getcertcheckurl='openssl x509 -noout -ocsp_uri -in'
function csrkeygen() {
    if [ "$#" -ne 2 ]; then
        echo "Usage: csrkeygen <path_to_csr> <path_to_key>"
        return 1
    fi

    local csr_path="$1"
    local key_path="$2"

    openssl req -newkey rsa:2048 -nodes -keyout "$key_path" -out "$csr_path"
    openssl req -text -noout -verify -in "$csr_path"
}

function csrkeygencn() {
    if [ "$#" -ne 3 ]; then
        echo "This will create a CSR and private key pair for the domain (e.g. domain.com."
        echo "If you wish to include all subdomains too, it should be of the form *.domain.com"
        echo "Usage: csrkeygencn <path_to_csr> <path_to_key> <domain>"
        return 1
    fi

    local csr_path="$1"
    local key_path="$2"
    local domain_name="$3"

    openssl req -newkey rsa:2048 -nodes -keyout "$key_path" -out "$csr_path" -subj "/CN=$domain_name"
    openssl req -text -noout -verify -in "$csr_path"
}