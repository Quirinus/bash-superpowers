#!/bin/bash

# Linux aliases
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/common-aliases
bashreload() {
    if [[ -f "${HOME}/.bash_profile" ]]; then
        source "${HOME}/.bash_profile"
        echo ".bash_profile sourced successfully!"
    elif [[ -f "${HOME}/.profile" ]]; then
        source "${HOME}/.profile"
        echo ".profile sourced successfully!"
    else
        echo "Neither .bash_profile nor .profile were found in your home directory."
    fi
}
aliasreload() {
    if [ -f ${HOME}/.aliases/.bash_aliases ]; then
        source ${HOME}/.aliases/.bash_aliases
    fi

    for file in ${HOME}/.aliases/.bash_aliases_*; do
        source "$file"
    done

    echo "Aliases reloaded."
}
aliases() {
    local aliases_dir="${HOME}/.aliases"
    local default_file=".bash_aliases"
    local file_to_edit=""

    # Display the help information
    display_help() {
        echo "Usage: aliases [suffix]"
        echo "Manage alias files in the ${HOME}/.aliases directory."
        echo
        echo "Options:"
        echo "  -h, --help   Show this help message and exit."
        echo
        echo "If no arguments are provided, edits the ${HOME}/.aliases/.bash_aliases file."
        echo "If a suffix is provided, edits ${HOME}/.aliases/.bash_aliases_SUFFIX."
    }

    # Check if the directory exists, if not create it
    if [[ ! -d $aliases_dir ]]; then
        mkdir -p "$aliases_dir"
    fi

    # Handle -h and --help flags
    if [[ $1 == "-h" || $1 == "--help" ]]; then
        display_help
        return 0
    fi

    # Decide which file to edit based on arguments
    if [[ $# -eq 0 ]]; then
        file_to_edit="${aliases_dir}/${default_file}"
    elif [[ $# -eq 1 ]]; then
        file_to_edit="${aliases_dir}/${default_file}_$1"
    else
        echo "Error: Invalid arguments."
        display_help
        return 1
    fi

    # Open the chosen file with nano -c
    nano -c "$file_to_edit"
}


# **** search/display aliases/functions ****
getDir() {
    # colors: https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
    # formatting: http://misc.flogisoft.com/bash/tip_colors_and_formatting
    # also: https://askubuntu.com/questions/528928/how-to-do-underline-bold-italic-strikethrough-color-background-and-size-i
    LightBlue='\033[1;34m'
    White='\e[97m'
    Underline='\e[4m'
    Bold='\e[1m'
    DarkGrayBackground='\e[100m'
    Green='\e[32m'
    Yellow='\e[33m'
    NoFormatting='\e[0m'

    #PS='${Green}[\h]${Yellow}$(__git_ps1 " (%s)")${NoFormatting} >> ' # host with git, and coloring
    #PS=$(__git_ps1 " [%s]") # just git
    #REALPATH=$(realpath .) # aboslute path
    REALPATH=$(realpath . | tr -d '\n') # absolute path with no newline at the end

    echo -e -n "${White}${Underline}${Bold}${REALPATH}${NoFormatting}\n";
    #echo -e -n "${White}${Underline}${Bold}${REALPATH}${NoFormatting}${Green}${PS}${NoFormatting}:\n";
}
alias ll='ls -avF --almost-all --color=always --group-directories-first' # --color=auto colors only when outputing to terminal, --color=always passes colors through pipe # TODO: Add permissions as a number in addition.
alias l='getDir && ll'
alias ld='getDir && ls -avFlhs --color=always --group-directories-first' # TODO: Add permissions as a number in addition.
alias c='clear && l'
alias grep='grep --color'
alias fgrep='fgrep --color'
alias egrep='egrep --color'
g() { fgrep -n -r -i --exclude=*.{css,js,log,txt} --include=*.* -e "$1" *.* .; } # fgrep all files excep .css, .js, .log, .txt  files and files that don't have an extension
gall() { fgrep -n -r -i -I -e "$1" *.* .; } # fgrep all files
fall() { find . -iname "*$1*" | grep -i -e "$1"; } # find some file or directory that contains string in the name, recursively, as a glob
f() { find . -maxdepth 1 -iname "*$1*" | grep -i -e "$1"; } # find some file or directory that contains string in the name, as a glob, with max depth 1
gf() { echo -e "\e[97m\e[4mFile and directory name matches:\e[0m" && fall "$1" && echo -e "\n\e[97m\e[4mContent matches:\e[0m" && g "$1"; } # fall and g
gfall() { echo -e "\e[97m\e[4mFile and directory name matches:\e[0m" && fall "$1" && echo -e "\n\e[97m\e[4mContent matches:\e[0m" && gall "$1"; } # fall and gall
gg() { echo -e "\e[97m\e[4mFile and directory name matches:\e[0m" && f "$1" && echo -e "\n\e[97m\e[4mContent matches:\e[0m" && g "$1"; }
get_between() { sed -n "${2},${3}p" "$1"; } # get lines from file between two line numbers
alias between='get_between'
alias countf='find . -type f | wc -l' # count files in directory and children
alias countd='dirscount=$(find . -type d | wc -l); echo $((--dirscount))' # count directories in directory and children
alias lfs='command ls -vogphaS --time-style=+"" | grep -v / | tr -s " " | cut -d" " -f3- | column -t' # show current directory file sizes and names
alias sizef='du -sh && lfs' # show size of the current directory and file sizes in current location
alias sized='du -d 1 -h | sort -rh' # show size of current directory and directories in current directory
alias sizeall='du -sh && du -d 1 -h | sort -rh | head -n -1 && lfs' # show size of current directory, files, and directories in current directory
alias timef='command ls -vogphat | grep -v / | tr -s " " | cut -d" " -f3- | column -t' # shows when files were created
alias timed='command ls -vloaght | grep "^d" | tr -s " " | cut -d" " -f4- | column -t' #'du -d 1 -h | sort -rh' # shows when directories were created
alias screen='TERM=screen-256color screen' # fixes tab autocomplete flashing in screen
t() { # With no arguments "t", lists tmux sessions, and with an arugment (number) "t 1", attaches the session with that number.
    if [ -z "$1" ]; then
        tmux list-sessions
    else
        tmux attach -t "$1"
    fi
}

# **** CD related aliases/functions ****
# shows directory path and content after cd, and adds wildcards at the start/end if needed,
# if theres multiple matches, shows them, if theres no matches, shows error
# there's a bug where it doesn't recognize partial matches in one case, todo: fix it
cd() {
    local dir="$1"

    # A function to suppress the output
    suppress_output() {
        "$@" > /dev/null 2>&1
    }

    # Try normally entering the directory
    suppress_output builtin cd "$dir" && {
        getDir
        ll
        return 0
    }

    # If directory exists but failed to enter
    if [ -d "$dir" ]; then
        getDir
        ll
        echo "Failed to enter directory: $dir"
        return 1
    fi

    # Try entering the directory with a wildcard
    suppress_output builtin cd *"$dir"* && {
        getDir
        ll
        return 0
    }

    # Since wildcard cd failed (probably multiple/no matches),
    # list all directories matching the wildcard pattern
    matches=$(ls -ld .*"$dir"* *"$dir"* 2>/dev/null)
    if [ $? -eq 0 ]; then
        getDir

        # Check if a single directory matches the wildcard
        [ -d *"$dir"* ] && {
            ll
            echo "Failed to enter directory with wildcard: *$dir*"
            return 1
        }

        # Multiple results exist, show them
        echo "$matches"
        echo "Multiple matches found, be more specific."
        return 1
    fi

    # Otherwise, no matching directory found
    getDir
    ll
    echo "No matching directory found: $dir"
    return 1
}
alias cd..='cd ..'
alias ..='cd ..'
alias ,,='cd -' # go back one directory (e.g. after doing cd .., enter the dir you exited again, instead of typing cd <dirname>)
alias ...='cd /'
alias root='cd /'
alias home='cd ${HOME}'
alias h='cd ${HOME}'

# **** general functions/aliases ****
alias sudo='sudo ' # this is so that other commands get expanded
s() { # Enter root user in the current directory, load root .bashrc, load current user aliases, change prompt (shows it's sudo and it's beautiful).
    sudo -i bash -c "cd '$(pwd)'; exec bash --rcfile <(
        echo '
        [[ -f ~/.bashrc ]] && source ~/.bashrc;
        [[ -f /home/$(id -un)/.bash_aliases ]] && source /home/$(id -un)/.bash_aliases;
        for file in /home/$(id -un)/.aliases/.bash_aliases_*; do
            [[ -f \"$file\" ]] && source \"$file\";
        done;
        PS1=\"\\[\\e[1;31m\\](sudo)\\[\\e[0m\\] \\[\\e[32m\\]>>\[\\e[0m\\] \";
        ')"
}
super-sudo() { # don't use this, use ssudo instead, so it runs aliases too
	# run functions, aliases, builtins, and executables in user's but not root's $PATH # https://unix.stackexchange.com/a/705011
    (($#)) || { echo "Usage: super-sudo CMD [ARGS...]" >&2; return 1; }
    local def ftype; ftype=$(type -t $1) ||
    { echo "not found: $1" >&2; return 1; }
    if [[ "$ftype" == "function" ]]; then def=$(declare -f "$1")
    else def=$(declare -p PATH); fi  # file or builtin
    sudo bash -c "${def};$(printf ' %q' "$@")"
}
alias ssudo='super-sudo ' # run stuff with sudo, that otherwise doesn't work with sudo (e.g. aliases, functions, ...)
# TODO: add commands like s and ssudo, but for another user
alias r='rm -rf'
mkdir_enter() { command mkdir -pv "$1"; cd "$1"; } # make directory and enter it. creates parent dirs if needed
alias mk='mkdir_enter'
alias wget='wget -c ' # continue
alias ping='ping -c 5' # limit to 5 pings
alias p='ps -f' # show currently executing processes
alias pu='ps -f -U $USER' # show currently executing processes for the user
alias listen='netstat -tulpn' # shows which processes listen to what
alias nano='nano -c' # nano with line numbers
per() { # short permission split by |
    if [ $# -eq 0 ]; then
        set -- .
    fi

    PERM=$(stat -c %A $1 | tr -d '\n')
    PERM_NUMBER=$(stat -c %a $1)

    DIR_OR_FILE=${PERM:0:1}
    USER_PERM=${PERM:1:3}
    GROUP_PERM=${PERM:4:3}
    OTHER_PERM=${PERM:7:3}

    echo "${DIR_OR_FILE}|${USER_PERM}|${GROUP_PERM}|${OTHER_PERM} (${PERM_NUMBER})";
}
perm() { # permissions with table header and help
    if [ "$1" == "--help" ]; then

        if [ $# -eq 0 ]; then
            set -- .
        fi

        echo "Shows the file/directory permissions.";
        echo "";
        echo "To get this help message you can use 'perm --help'.";
        echo "";
        echo "Usage: perm FILE";
        echo "";
        echo "Legend:";
        echo -e "${Underline}D|Usr|Grp|Oth (Num)${NoFormatting}";
        echo "";
        echo -e "D (Directory): d/- = ${Underline}d${NoFormatting}irectory/file";
        echo -e "Usr (User), Grp (Group), Oth (Others): -/r/w/x = no/${Underline}r${NoFormatting}ead/${Underline}w${NoFormatting}rite/e${Underline}x${NoFormatting}ecute permission";
        echo -e "Num (Numeric permission): ### (three digits)";
        return 0;
    fi

    Underline='\e[4m'
    NoFormatting='\e[0m'
    White='\e[97m'
    Bold='\e[1m'

    echo -e "${White}Permissions:${NoFormatting}";
    echo -e "${Underline}D|Usr|Grp|Oth (Num)${NoFormatting}";
    per "$1";
}
owner() {
    if [ $# -eq 0 ]; then
        set -- .
    fi

    OWNER_AND_GROUP=$(stat -c "%U:%G" $1)

    Underline='\e[4m'
    NoFormatting='\e[0m'
    White='\e[97m'
    Bold='\e[1m'

    echo -e "${White}Owner:${NoFormatting}";
    echo -e "${Underline}user:group${NoFormatting}";
    echo "${OWNER_AND_GROUP}";
}
alias user='owner'
alias owners='owner'
alias group='owner'
own() { # Claim ownership of files/dirs. If no argument, current directory is used.
    if [ $# -eq 0 ]; then
        set -- .
    fi

    sudo chown -R "$(id -un):$(id -gn)" "$@"
}
www() { # Give ownership of files/dirs to www-data. If no argument, current directory is used.
    if [ $# -eq 0 ]; then
        set -- .
    fi

    sudo chown -R www-data:www-data "$@"
}
# extracts various archives
# more advanced version found here:
# https://github.com/xvoland/Extract/blob/master/extract.sh
e() {
    if [ -f "$1" ] ; then
        case $1 in
            *.tar.bz2)   tar xvjf "$1" ;;
            *.tar.gz)    tar xvzf "$1" ;;
            *.tar.xz)    tar xvJf "$1" ;;
            *.bz2)       bunzip2 "$1" ;;
            *.rar)       unrar x "$1" ;;
            *.gz)        gunzip "$1" ;;
            *.tar)       tar xvf "$1" ;;
            *.tbz2)      tar xvjf "$1" ;;
            *.tgz)       tar xvzf "$1" ;;
            *.zip)       unzip "$1" ;;
            *.jar)       unzip "$1" ;;
            *.Z)         uncompress "$1" ;;
            *.7z)        7z x "$1" ;;
            *)           echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}
alias targz='command tar -czf' # followed by "archive filename" "source path" --> creates a .tar.gz file
alias tar='command tar -cf' # followed by "archive filename" "source path" --> creates a .tar file
alias error='sudo tail --lines=20 /var/log/apache2/error.log | awk '\''{gsub(/[\n[]/, "\n- [")};1'\'' | awk '\''{gsub(/\\n/, "\n")};1'\'' | awk '\''{gsub(/ \#0/, "\n#0")};1'\''' # gets last 10 apache errors, and formats them. change the file name/location to use

# **** time/date related aliases ****
alias nowutc='date +"%T %A %d-%m-%Y %B" --utc'
alias utc='nowutc'
alias timeutc='nowutc'
alias nowserver='date +"%T %A %d-%m-%Y %B %Z %z"'
alias timeserver='nowserver'
alias now=$'TZ=\'Europe/Zagreb\' date +"%T %A %d-%m-%Y %B %Z %z"'
alias timer='command time'
alias time='now'
alias timestamp='date +%s' # unix timestamp

# **** hashing ****
pass() { openssl rand -base64 $1 | tr -d '\n' | cut -b 1-$1; } # generate semi-random pass of some length, example: getpass 20
alias sha256='shasum -a 256'
alias sha1='sha1sum '
alias md5='md5sum '
integrity() { echo 'sha384-'$(cat "$1" | openssl dgst -sha384 -binary | openssl base64 -A); } # gets the integrity hash for js/css to use in attributes. example: integrity jquery-3.5.1.slim.min.js

